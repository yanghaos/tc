# SQLEngine

SQLEngine SIG 致力于提升openGauss的SQL支持能力、执行效率，为用户提供丰富语法支持，满足用户多方面业务需求。

# 组织会议

- 公开的会议时间：北京时间 每双周四 16:00-17:00

# 成员

### Maintainer列表

- 彭炯[@totaj](https://gitee.com/totaj)，*pengjiong1@huawei.com*
- 杨皓[@yangahos](https://gitee.com/yanghaos)，*yanghao37@huawei.com*
- 岳行之[@gaussyuexz](https://gitee.com/gaussyuexz)，*yuexingzhi2@huawei.com*
- 周兆琦[@ziki77](https://gitee.com/ziki77)，*zhouzhaoqi1@huawei.com*

### Committer列表

- 蔡磊[@cailei19](https://gitee.com/蔡磊19)，*cailei19@huawei.com*
- 胡正超[@gentle_hu](https://gitee.com/gentle_hu), *huzhengchao4@huawei.com*
- 李海啸[@tsunamis-li](https://gitee.com/tsunamis-li)，*lihaixiao3@huawei.com*
- 王碧华[@bihua111](https://gitee.com/bihua111)，*wangbihua1@huawei.com*
- 吴禹均[@wuyujun3](https://gitee.com/wuyujun3)，*wuyujun2@huawei.com*
- 章佳豪[@zhangjiahao_hw](https://gitee.com/zhangjiahao_hw)，*zhangjiahao65@huawei.com*
- 张静昌[@jc-zhang](https://gitee.com/jc-zhang)，*zhangjingchang1@huawei.com*
- 赖盛好[@dodders](https://gitee.com/dodders)，*laishenghao1@huawei.com*
- 杨志铮[@yangzhizheng94](https://gitee.com/yangzhizheng94)，*yangzhizheng@chinamobile.com*
- 赵立伟[@levy5307](https://gitee.com/levy5307)，*zhaoliwei@chinamobile.com*
- 柳长沣[@superblaker](https://gitee.com/superblaker)，*liuchangfeng2@huawei.com*

# 联系方式

- [邮件列表](https://mailweb.opengauss.org/postorius/lists/sqlengine.opengauss.org/)

# 仓库清单

仓库地址：

- https://gitee.com/opengauss/openGauss-server

- https://gitee.com/opengauss/openGauss-third_party
